package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AuthAbstractCommand;

public class AuthLogoutCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout app";
    }

    @Override
    public void execute() {
        System.out.println("Logout app");
        serviceLocator.getAuthService().logout();
    }
}
