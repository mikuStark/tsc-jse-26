package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.model.AbstractEntity;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

import java.util.List;
import java.util.Comparator;
import java.util.ArrayList;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NonNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return new ArrayList<>(entities);
    }

    @Override
    public void remove(final @NonNull E entity) {
        entities.remove(entity.getId());
    }
}
